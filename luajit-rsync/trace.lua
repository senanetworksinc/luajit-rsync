local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {
	level = {
		RS_LOG_EMERG = rsync.RS_LOG_EMERG,
		RS_LOG_ALERT = rsync.RS_LOG_ALERT,
		RS_LOG_CRIT = rsync.RS_LOG_CRIT,
		RS_LOG_ERR = rsync.RS_LOG_ERR,
		RS_LOG_WARNING = rsync.RS_LOG_WARNING,
		RS_LOG_NOTICE = rsync.RS_LOG_NOTICE,
		RS_LOG_INFO = rsync.RS_LOG_INFO,
		RS_LOG_DEBUG = rsync.RS_LOG_DEBUG,
	}
}

function _M.set_level(level)
	rsync.rs_trace_set_level(level)
end

function _M.to(callback)
	local cb = ffi.cast("rs_trace_fn_t*", callback)

	rsync.rs_trace_to(to)

	return cb
end

function _M.stderr(level, msg)
	rsync.rs_trace_stderr(level, msg)
end

function _M.supports_trace(msg)
	return rsync.rs_supports_trace()
end

return _M