local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.stats()
	local stats = ffi.new("rs_stats_t")

	ffi.fill(stats, ffi.sizeof("rs_stats_t"))

	rsync.rs_log_stats(stats)

	return stats
end

return _M