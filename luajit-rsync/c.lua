local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.rs_trace_set_level(level)
	return rsync.rs_trace_set_level(level)
end

function _M.rs_trace_to(callback)
	return rsync.rs_trace_to(callback)
end

function _M.rs_trace_stderr(level, msg)
	return rsync.rs_trace_stderr(level, ffi.cast("char const *", msg))
end

function _M.rs_supports_trace()
	return rsync.rs_supports_trace()
end

function _M.rs_unbase64(s)
	return rsync.rs_unbase64(ffi.cast("char*", s))
end

function _M.rs_base64(buffer, n, out)
	return rsync.rs_base64(ffi.cast("unsigned char const *", buffer), n, out)
end

function _M.rs_strerror(r)
	return rsync.rs_strerror(r)
end

function _M.rs_mdfour(out, invalue, size)
	return rsync.rs_mdfour(ffi.cast("unsigned char *", out), ffi.cast("void const*", invalue), size)
end

function _M.rs_mdfour_begin(md)
	return rsync.rs_mdfour_begin(ffi.cast("rs_mdfour_t*", md))
end

function _M.rs_mdfour_update(md, in_void, n)
	return rsync.rs_mdfour_update(ffi.cast("rs_mdfour_t*", md), ffi.cast("void const *", in_void), n)
end

function _M.rs_mdfour_result(md, out)
	return rsync.rs_mdfour_result(ffi.cast("rs_mdfour_t*", md), ffi.cast("unsigned char *", out))
end

function _M.rs_format_stats(stats, buf, size)
	return rsync.rs_format_stats(ffi.cast("rs_stats_t const *", stats), buf, size)
end

function _M.rs_log_stats(stats)
	return rsync.rs_log_stats(ffi.cast("rs_stats_t const *", stats))
end

function _M.rs_signature_log_stats(sig)
	return rsync.rs_signature_log_stats(ffi.cast("rs_signature_t const *", sig))
end

function _M.rs_free_sumset(sig)
	return rsync.rs_free_sumset(ffi.cast("rs_signature_t*", sig))
end

function _M.rs_sumset_dump(sig)
	return rsync.rs_sumset_dump(ffi.cast("rs_signature_t const*", sig))
end

function _M.rs_job_iter(job, buffers)
	return rsync.rs_job_iter(ffi.cast("rs_job_t*", job), ffi.cast("rs_buffers_t*", buffers))
end

function _M.rs_job_drive(job, buf, in_cb, in_opaque, out_cb, out_opaque)
	return rsync.rs_job_drive(ffi.cast("rs_job_t*", job), ffi.cast("rs_buffers_t*", buf), in_cb, ffi.cast("void*", in_opaque), out_cb, ffi.cast("void*", out_opaque))
end

function _M.rs_job_statistics(job)
	return rsync.rs_job_statistics(ffi.cast("rs_job_t*", job))
end

function _M.rs_job_free(job)
	return rsync.rs_job_free(ffi.cast("rs_job_t*", job))
end

function _M.rs_sig_args(old_fsize, magic, block_len, strong_len)
	return rsync.rs_sig_args(old_fsize, ffi.cast("rs_magic_number*", magic), ffi.cast("size_t*", block_len), ffi.cast("size_t*", strong_len))
end

function _M.rs_sig_begin(block_len, strong_len, sig_magic)
	return rsync.rs_sig_begin(block_len, strong_len, sig_magic)
end

function _M.rs_delta_begin(sig)
	return rsync.rs_delta_begin(ffi.cast("rs_signature_t*", sig))
end

function _M.rs_loadsig_begin(sig)
	return rsync.rs_loadsig_begin(ffi.cast("rs_signature_t**", sig))
end

function _M.rs_build_hash_table(sig)
	return rsync.rs_build_hash_table(ffi.cast("rs_signature_t*", sig))
end

function _M.rs_patch_begin(copy_cb, copy_arg)
	return rsync.rs_patch_begin(copy_cb, ffi.cast("void*", copy_arg))
end

function _M.rs_file_open(filename, mode, force)
	return rsync.rs_file_open(ffi.cast("char const*", filename), ffi.cast("char const*", mode), force)
end

function _M.rs_file_close(file)
	return rsync.rs_file_close(ffi.cast("FILE*", file))
end

function _M.rs_file_size(file)
	return rsync.rs_file_size(ffi.cast("FILE*", file))
end

function _M.rs_file_copy_cb(arg, pos, len, buf)
	return rsync.rs_file_copy_cb(ffi.cast("void*", arg), pos, ffi.cast("size_t*", len), ffi.cast("void**", buf))
end

function _M.rs_sig_file(old_file, sig_file, block_len, strong_len, sig_magic, stats)
	return rsync.rs_sig_file(ffi.cast("FILE*", old_file), ffi.cast("FILE*", sig_file), block_len, strong_len, sig_magic, ffi.cast("rs_stats_t*", stats))
end

function _M.rs_loadsig_file(sig_file, sumset, stats)
	return rsync.rs_loadsig_file(ffi.cast("FILE*", sig_file), ffi.cast("rs_signature_t**", sumset), ffi.cast("rs_stats_t*", stats))
end

function _M.rs_delta_file(sig, new_file, delta_file, stats)
	return rsync.rs_delta_file(ffi.cast("rs_signature_t*", sig), ffi.cast("FILE*", new_file), ffi.cast("FILE*", delta_file), ffi.cast("rs_stats_t*", stats))
end

function _M.rs_patch_file(basis_file, delta_file, new_file, stats)
	return rsync.rs_patch_file(ffi.cast("FILE*", basis_file), ffi.cast("FILE*", delta_file), ffi.cast("FILE*", new_file), ffi.cast("rs_stats_t*", stats))
end

return _M