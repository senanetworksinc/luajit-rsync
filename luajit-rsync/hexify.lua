local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.hexify(buffer)
	local out_size = #buffer * 2 + 1

	local out = ffi.new("char[" .. tostring(out_size) .. "]")

	ffi.fill(out, out_size)

	rsync.rs_hexify(out, buffer, #buffer)

	return ffi.string(out)
end

return _M