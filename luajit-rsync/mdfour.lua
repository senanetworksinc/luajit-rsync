local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.mdfour(invalue, size)
	local tmp = ffi.new("unsigned char[256]")

	ffi.fill(tmp, 256)

	rsync.rs_mdfour(tmp, ffi.cast("void const*", invalue), size)

	return ffi.string(tmp)
end

function _M.mdfour_t()
	return ffi.new("rs_mdfour_t")
end

function _M.begin(md)
	rsync.rs_mdfour_begin(ffi.cast("rs_mdfour_t*", md))
end

function _M.update(md, in_void, n)
	rsync.rs_mdfour_update(ffi.cast("rs_mdfour_t*", md), ffi.cast("void const*", in_void), n)
end

function _M.result(md)
	local tmp = ffi.new("unsigned char[256]")

	ffi.fill(tmp, 256)

	rsync.rs_mdfour_result(ffi.cast("rs_mdfour_t*", md), ffi.cast("unsigned char*", tmp))

	return ffi.string(tmp)
end

return _M