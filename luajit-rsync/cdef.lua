local ffi = require "ffi"
local loaded, rsync = pcall(ffi.load, "rsync")

if not loaded then
	loaded, rsync = pcall(ffi.load, "librsync")

	if not loaded then
		loaded, rsync = pcall(ffi.load, "/usr/local/lib/librsync")

		if not loaded then
			error("can't load rsync!")
		end
	end
end

ffi.cdef[[
extern char const rs_librsync_version[];

typedef uint8_t rs_byte_t;

typedef long long rs_long_t;

typedef signed long long time_t;

typedef enum {
	RS_DELTA_MAGIC = 0x72730236,
	RS_MD4_SIG_MAGIC = 0x72730136,
	RS_BLAKE2_SIG_MAGIC = 0x72730137,
	RS_RK_MD4_SIG_MAGIC = 0x72730146,
	RS_RK_BLAKE2_SIG_MAGIC = 0x72730147,
} rs_magic_number;

typedef enum {
	RS_LOG_EMERG = 0,
	RS_LOG_ALERT = 1,
	RS_LOG_CRIT = 2,
	RS_LOG_ERR = 3,
	RS_LOG_WARNING = 4,
	RS_LOG_NOTICE = 5,
	RS_LOG_INFO = 6,
	RS_LOG_DEBUG = 7
} rs_loglevel;

typedef void rs_trace_fn_t(rs_loglevel level, char const *msg);

void rs_trace_set_level(rs_loglevel level);

void rs_trace_to(rs_trace_fn_t *);

void rs_trace_stderr(rs_loglevel level, char const *msg);

int rs_supports_trace(void);

void rs_hexify(char *to_buf, void const *from_buf,
							int from_len);

size_t rs_unbase64(char *s);

void rs_base64(unsigned char const *buf, int n, char *out);

typedef enum rs_result {
	RS_DONE = 0,
	RS_BLOCKED = 1,
	RS_RUNNING = 2,
	RS_TEST_SKIPPED = 77,
	RS_IO_ERROR = 100,
	RS_SYNTAX_ERROR = 101,
	RS_MEM_ERROR = 102,
	RS_INPUT_ENDED = 103,
	RS_BAD_MAGIC = 104,
	RS_UNIMPLEMENTED = 105,
	RS_CORRUPT = 106,
	RS_INTERNAL_ERROR = 107,
	RS_PARAM_ERROR = 108
} rs_result;

char const *rs_strerror(rs_result r);

typedef struct rs_stats {
    char const *op;             /**< Human-readable name of current operation.
                                 * For example, "delta". */
    int lit_cmds;               /**< Number of literal commands. */
    rs_long_t lit_bytes;        /**< Number of literal bytes. */
    rs_long_t lit_cmdbytes;     /**< Number of bytes used in literal command
                                 * headers. */

    rs_long_t copy_cmds, copy_bytes, copy_cmdbytes;
    rs_long_t sig_cmds, sig_bytes;
    int false_matches;

    rs_long_t sig_blocks;       /**< Number of blocks described by the
                                 * signature. */

    size_t block_len;

    rs_long_t in_bytes;         /**< Total bytes read from input. */
    rs_long_t out_bytes;        /**< Total bytes written to output. */

    time_t start, end;
} rs_stats_t;

struct rs_mdfour {
	unsigned int A, B, C, D;
	uint64_t totalN;
	int tail_len;
	unsigned char tail[64];
};

typedef struct rs_mdfour rs_mdfour_t;

extern const int RS_MD4_SUM_LENGTH, RS_BLAKE2_SUM_LENGTH;

enum { RS_MAX_STRONG_SUM_LENGTH = 32 };

typedef uint32_t rs_weak_sum_t;
typedef unsigned char rs_strong_sum_t[RS_MAX_STRONG_SUM_LENGTH];

void rs_mdfour(unsigned char *out, void const *in, size_t);
void rs_mdfour_begin( /* @out@ */ rs_mdfour_t *md);

void rs_mdfour_update(rs_mdfour_t *md, void const *in_void,
                                      size_t n);
void rs_mdfour_result(rs_mdfour_t *md, unsigned char *out);

char *rs_format_stats(rs_stats_t const *stats, char *buf,
                                      size_t size);

int rs_log_stats(rs_stats_t const *stats);

typedef struct hashtable {
	int size;
	int count;
	unsigned tmask;
	unsigned bshift;
	long find_count;
	long match_count;
	long hashcmp_count;
	long entrycmp_count;
	unsigned char *kbloom;
	void **etable;
	unsigned ktable[];
} hashtable_t;

struct rs_signature {
	int magic;
	int block_len;
	int strong_sum_len;
	int count;
	int size;
	void *block_sigs;
	hashtable_t *hashtable;
	long calc_strong_count;
};

typedef struct rs_signature rs_signature_t;

void rs_signature_log_stats(rs_signature_t const *sig);

void rs_free_sumset(rs_signature_t *);

void rs_sumset_dump(rs_signature_t const *);

struct rs_buffers_s {
	char *next_in;
	size_t avail_in;
	int eof_in;
	char *next_out;
	size_t avail_out;
};

typedef struct rs_buffers_s rs_buffers_t;

typedef rs_result rs_copy_cb(void *opaque, rs_long_t pos, size_t *len,
							void **buf);

enum { RS_DEFAULT_BLOCK_LEN = 2048 };

enum { RS_DEFAULT_MIN_STRONG_LEN = 12 };

typedef enum {
	RS_ROLLSUM,
	RS_RABINKARP,
} weaksum_kind_t;

typedef struct _Rollsum {
	size_t count;
	uint16_t s1;
	uint16_t s2;
} Rollsum;

typedef struct _rabinkarp {
	size_t count;
	uint32_t hash;
	uint32_t mult;
} rabinkarp_t;

typedef struct weaksum {
	weaksum_kind_t kind;
	union {
		Rollsum rs;
		rabinkarp_t rk;
	} sum;
} weaksum_t;

typedef struct rs_job rs_job_t;

struct rs_job {
	int dogtag;
	const char *job_name;
	rs_buffers_t *stream;
	rs_result (*statefn)(rs_job_t *);
	rs_result final_result;
	int sig_magic;
	int sig_block_len;
	int sig_strong_len;
	rs_long_t sig_fsize;
	rs_signature_t *signature;
	int job_owns_sig;
	unsigned char op;
	rs_weak_sum_t weak_sig;
	weaksum_t weak_sum;
	rs_long_t param1, param2;
	struct rs_prototab_ent const *cmd;
	rs_mdfour_t output_md4;
	rs_stats_t stats;
	rs_byte_t *scoop_buf;
	rs_byte_t *scoop_next;
	size_t scoop_alloc;
	size_t scoop_avail;
	size_t scoop_pos;
	rs_byte_t write_buf[36];
	size_t write_len;
	size_t copy_len;
	rs_long_t basis_pos, basis_len;
	rs_copy_cb *copy_cb;
	void *copy_arg;
};

rs_result rs_job_iter(rs_job_t *job, rs_buffers_t *buffers);

typedef rs_result rs_driven_cb(rs_job_t *job, rs_buffers_t *buf, void *opaque);

rs_result rs_job_drive(rs_job_t *job, rs_buffers_t *buf, rs_driven_cb in_cb, void *in_opaque, rs_driven_cb out_cb, void *out_opaque);

const rs_stats_t *rs_job_statistics(rs_job_t *job);

rs_result rs_job_free(rs_job_t *);

rs_result rs_sig_args(rs_long_t old_fsize, rs_magic_number * magic, size_t *block_len, size_t *strong_len);

rs_job_t *rs_sig_begin(size_t block_len, size_t strong_len, rs_magic_number sig_magic);

rs_job_t *rs_delta_begin(rs_signature_t *);

rs_job_t *rs_loadsig_begin(rs_signature_t **);

rs_result rs_build_hash_table(rs_signature_t *sums);

rs_job_t *rs_patch_begin(rs_copy_cb * copy_cb, void *copy_arg);

]]


if 1 then
ffi.cdef[[
typedef struct {} FILE;

FILE *rs_file_open(char const *filename, char const *mode, int force);

int rs_file_close(FILE *file);

rs_long_t rs_file_size(FILE *file);

rs_result rs_file_copy_cb(void *arg, rs_long_t pos, size_t *len, void **buf);

extern int rs_inbuflen, rs_outbuflen;

rs_result rs_sig_file(FILE *old_file, FILE *sig_file, size_t block_len, size_t strong_len, rs_magic_number sig_magic, rs_stats_t *stats);

rs_result rs_loadsig_file(FILE *sig_file, rs_signature_t **sumset, rs_stats_t *stats);

rs_result rs_delta_file(rs_signature_t *, FILE *new_file, FILE *delta_file, rs_stats_t *);

rs_result rs_patch_file(FILE *basis_file, FILE *delta_file, FILE *new_file, rs_stats_t *);
]]
end

return rsync