local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.base64(buffer)
	local out_size = #buffer * 4 / 3 + 3 + 1

	local out = ffi.new("char[" .. tostring(out_size) .. "]")

	ffi.fill(out, out_size)

	rsync.rs_base64(ffi.cast("unsigned char const *", buffer), #buffer, out)

	if not out then
		return nil
	else
		return ffi.string(out)
	end
end

return _M