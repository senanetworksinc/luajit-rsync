local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.unbase64(buffer)
	local input_size = #buffer + (#buffer * 3 / 4 + 2 + 1)

	local input = ffi.new("char[" .. input_size .. "]")

	ffi.fill(input, input_size)

	ffi.copy(input, buffer, #buffer)

	local len = rsync.rs_unbase64(input)

	return ffi.string(input, len)
end

return _M