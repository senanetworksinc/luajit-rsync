local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {}

function _M.signature()
	local sig = ffi.new("rs_signature_t")

	ffi.fill(sig, ffi.sizeof("rs_signature_t"))

	rsync.rs_signature_log_stats(sig)

	return sig
end

return _M