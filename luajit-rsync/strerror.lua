local rsync = require "luajit-rsync"
local ffi = require "ffi"

local _M = {
	RS_DONE = rsync.RS_DONE,
	RS_BLOCKED = rsync.RS_BLOCKED,
	RS_RUNNING = rsync.RS_RUNNING,
	RS_TEST_SKIPPED = rsync.RS_TEST_SKIPPED,
	RS_IO_ERROR = rsync.RS_IO_ERROR,
	RS_SYNTAX_ERROR = rsync.RS_SYNTAX_ERROR,
	RS_MEM_ERROR = rsync.RS_MEM_ERROR,
	RS_INPUT_ENDED = rsync.RS_INPUT_ENDED,
	RS_BAD_MAGIC = rsync.RS_BAD_MAGIC,
	RS_UNIMPLEMENTED = rsync.RS_UNIMPLEMENTED,
	RS_CORRUPT = rsync.RS_CORRUPT,
	RS_INTERNAL_ERROR = rsync.RS_INTERNAL_ERROR,
	RS_PARAM_ERROR = rsync.RS_PARAM_ERROR,
}

function _M.strerror(result)

	local r = rsync.rs_strerror(result)

	if not r then
		return nil
	else
		return ffi.string(r)
	end
end

return _M