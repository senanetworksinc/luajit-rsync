# luajit-rsync


[librsync](https://github.com/librsync/librsync)のバインディングです。

# Install
`$ luarocks install luajit-rsync`

# Usage


## base64

rs_base64()のラッパーです。

入力されたバイナリをbase64エンコードして返します。

```lua
local base64 = require "luajit-rsync.base64"

local input = "あいうえお"

local output = "44GC44GE44GG44GI44GK"

assert(base64.base64(input) == output)
```

## unbase64

rs_unbase64()のラッパーです。

入力されたstringをbase64デコードして返します。

```lua
local unbase64 = require "luajit-rsync.unbase64"

local input = "44GC44GE44GG44GI44GK"

local output = "あいうえお"

assert(unbase64.unbase64(input) == output)
```

## strerror

rs_strerror()のラッパーです。

入力されたrs_resultに応じた文字列を返します。

```lua
local strerror = require "luajit-rsync.strerror"

assert(strerror.strerror(strerror.RS_DONE) == "OK")
assert(strerror.strerror(strerror.RS_BLOCKED) == "blocked waiting for input or output buffers")
assert(strerror.strerror(strerror.RS_RUNNING) == "still running")
assert(strerror.strerror(strerror.RS_TEST_SKIPPED) == "unexplained problem")
assert(strerror.strerror(strerror.RS_IO_ERROR) == "IO error")
assert(strerror.strerror(strerror.RS_SYNTAX_ERROR) == "bad command line syntax")
assert(strerror.strerror(strerror.RS_MEM_ERROR) == "out of memory")
assert(strerror.strerror(strerror.RS_INPUT_ENDED) == "unexpected end of input")
assert(strerror.strerror(strerror.RS_BAD_MAGIC) == "bad magic number at start of stream")
assert(strerror.strerror(strerror.RS_UNIMPLEMENTED) == "unimplemented case")
assert(strerror.strerror(strerror.RS_CORRUPT) == "stream corrupt")
assert(strerror.strerror(strerror.RS_INTERNAL_ERROR) == "library internal error")
assert(strerror.strerror(strerror.RS_PARAM_ERROR) == "unexplained problem")
```

## trace

rsync.rs_trace_set_level、rsync.rs_trace_to、rsync.rs_trace_stderr、rsync.rs_supports_traceのラッパーです。

```lua
local trace = require "luajit-rsync.trace"

trace.set_level(trace.level.RS_LOG_EMERG)
trace.set_level(trace.level.RS_LOG_ALERT)
trace.set_level(trace.level.RS_LOG_CRIT)
trace.set_level(trace.level.RS_LOG_ERR)
trace.set_level(trace.level.RS_LOG_WARNING)
trace.set_level(trace.level.RS_LOG_NOTICE)
trace.set_level(trace.level.RS_LOG_INFO)
trace.set_level(trace.level.RS_LOG_DEBUG)

trace.supports_trace()

trace.stderr(trace.level.RS_LOG_DEBUG, "log")
```

## hexify

rs_hexify()のラッパーです。

入力されたstringを16進数表記に変換します。

```lua
local hexify = require "luajit-rsync.hexify"

local input = "1234567890"

local output = "31323334353637383930"

assert(hexify.hexify(input) == output)
```

## log

rs_log_statsのラッパーです。

```lua
local log = require "luajit-rsync.log"

local st = log.stats()

print(tostring(st.op or ""))
print(tonumber(st.lit_cmds))
print(tonumber(st.lit_bytes))
print(tonumber(st.lit_cmdbytes))
print(tonumber(st.copy_cmds))
print(tonumber(st.copy_bytes))
print(tonumber(st.copy_cmdbytes))
print(tonumber(st.sig_cmds))
print(tonumber(st.sig_bytes))
print(tonumber(st.false_matches))
print(tonumber(st.sig_blocks))
print(tonumber(st.block_len))
print(tonumber(st.in_bytes))
print(tonumber(st.out_bytes))
print(tonumber(st.start))
print(tonumber(st["end"]))
```

## mdfour

rs_mdfour、rs_mdfour_begin、rs_mdfour_update、rs_mdfour_resultのラッパーです。

```lua
local mdfour = require "luajit-rsync.mdfour"
local hexify = require "luajit-rsync.hexify"

local invalue = "1234"

local invalue_size = #invalue

assert(hexify.hexify(mdfour.mdfour(invalue, invalue_size)) == "f375f401ddc698af533f16f8ac1e91c1")

local md = mdfour.mdfour_t()

mdfour.begin(md)

mdfour.update(md, invalue, invalue_size)

assert(hexify.hexify(mdfour.result(md)) == "f375f401ddc698af533f16f8ac1e91c1")
```

## signature

rs_signature_log_statsのラッパーです。

```lua
local signature = require "luajit-rsync.signature"

signature.signature()
```

## cdef

librsyc全てのインターフェースが使用可能です。

型キャスト、メモリ管理などは呼び出し側で行う必要があります。

```lua
local rsync = require "luajit-rsync.cdef"

```

## c

librsyc全てのインターフェースが使用可能です。

luajit-rsync.cdefとは違い型キャストを実装済みです。

メモリ管理は呼び出し側で行う必要があります。

```lua
local rsync = require "luajit-rsync.c"

```


# Revesion

* 2021/11/22 1.0-0 release
